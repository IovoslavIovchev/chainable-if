# chainable-if

[![Crate](https://img.shields.io/crates/v/chainable-if.svg)](https://crates.io/crates/chainable-if)
[![Crate](https://img.shields.io/crates/l/chainable-if)](https://crates.io/crates/chainable-if)
[![docs.rs](https://docs.rs/chainable-if/badge.svg)](https://docs.rs/chainable-if/)

This crate provides the `if_chain!` macro -- a macro for composing
long chains of if-else if-else statements.

It is intended to provide an alternative to writing if chains with `match`,
such as:

```rust
match () {
    _ if some_condition => ...,
    _ if some_other_condition => ...,
    _ if some_third_condition => ...,
    _ => ...,
}
```

# Example usage

While it is mainly intended for long chains, `if_chain!` can be used
for simple if-else situations, such as:

```rust
if_chain! {
| some_condition => (/* some_condition is true */),
| _ => (/* the else branch */)
}
```

The earlier `match` example can be rewritten as:

```rust
if_chain! {
| some_condition => ...,
| some_other_condition => ...,
| some_third_condition => ...,
}
```

Note that the `else` branch is not required, since `if_chain!` simply gets
expanded into:

```rust
if some_condition {
    ...
} else if some_other_condition }
    ...
} else if some_third_condition {
    ...
}
```
